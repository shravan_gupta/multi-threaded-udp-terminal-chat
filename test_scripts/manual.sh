#!/bin/bash

if [ $# -ne 3 ] ; then
    echo "Usage:"
    echo "    $0 <which end> <path to student dir> <input file>"
    echo
    echo "Examples without using slow_cat:"
    echo "    $0 rawA ../as3/studentx unused       (Used for ports 12345->54321)"
    echo "    $0 rawB ../as3/studentx unused       (Used for ports 54321->12345)"
    echo
    echo "Example using slow_cat:"
    echo "    $0 A ../as3/studentx ./inputs/hw-1.txt"
    echo "    $0 B ../as3/studentx ./inputs/blank.txt"
    echo "Note: Must start up end 'a' first, then run for end 'b'."
    exit
fi


which_end=$1
stalk_path=$2
input_path=$3

if [ "$which_end" = "rawA" ] ; then
    echo "Manual test -- NO INPUT FIRST END"
    # Execute 
    $stalk_path/s-talk 12345 localhost 54321
    
elif [ "$which_end" = "rawB" ] ; then
    echo "Manual test -- NO INPUT SECOND END"
    # Start end B
    $stalk_path/s-talk 54321 localhost 12345


elif [ "$which_end" = "A" ] ; then
    echo "Manual test -- FIRST END"
    # Cleanup anything left over
    killall slow_cat s-talk 2> /dev/null
    sleep 1
    # Prep start semaphore
    ./slow_cat init
    sleep 2
    # Execute (wait for sigal)
    ./slow_cat $input_path 1000 1 500 | $stalk_path/s-talk 12345 localhost 54321
    
elif [ "$which_end" = "B" ] ; then
    echo "Manual test -- SECOND END"
    # Send start signal in 3 seconds
    ./signal_in_3s.sh &
    # Start end B
    ./slow_cat $input_path 1500 1 500 | $stalk_path/s-talk 54321 localhost 12345
else
    echo "Unknown 'which end': choose A, B, rawA, or rawB"
fi