#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <assert.h>
#include <stdint.h>
#include <errno.h>
#include <string.h>
#include <semaphore.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <pwd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <time.h>
#include <stdbool.h>
#include <signal.h>


static void sleep_ms(int duration_ms);


#define SIGNAL_NAME_WAIT "/cmpt300_slow_cat_wait"
#define SIGNAL_NAME_DONE "/cmpt300_slow_cat_done"

static bool is_done = false;

static void initialize_sem()
{
    // Create semaphores
    // -------------------------------------------------------------
    // NOTE: with a static linked binary sem_unlink segfaults
    // See bug: https://stackoverflow.com/a/47914897/3475174
    if (sem_unlink(SIGNAL_NAME_WAIT) != 0 && errno != ENOENT) {
        fprintf(stderr, "ERROR: Unable to unlink the WAIT semaphore!\n");
        exit(1);
    }
    if (sem_unlink(SIGNAL_NAME_DONE) != 0 && errno != ENOENT) {
        fprintf(stderr, "ERROR: Unable to unlink the DONE semaphore!\n");
        exit(1);
    }

    // ..shared (named) semaphores.
    sem_t *sem = sem_open(SIGNAL_NAME_WAIT, O_CREAT, 0644, 0);
    sem_close(sem);
    sem = sem_open(SIGNAL_NAME_DONE, O_CREAT, 0644, 0);
    sem_close(sem);
}
static void wait_till_signaled()
{
	// Wait on signal; resignal so others get it too.
	sem_t *sem = sem_open(SIGNAL_NAME_WAIT, 0);
	sem_wait(sem);
	sem_post(sem);
	sem_close(sem);
}
static void signal_other_procs()
{
	sem_t *sem = sem_open(SIGNAL_NAME_WAIT, 0);
    sem_post(sem);
    sem_close(sem);
}


static void wait_till_done()
{
    if (is_done) {
        return;
    }

	// Wait on signal; resignal so others get it too.
	sem_t *sem = sem_open(SIGNAL_NAME_DONE, 0);
	sem_wait(sem);
	sem_post(sem);
	sem_close(sem);
}
static void signal_done()
{
	sem_t *sem = sem_open(SIGNAL_NAME_DONE, 0);
    sem_post(sem);
    sem_close(sem);
}


static bool has_linefeed(char* str) 
{
    for (int i = 0; str[i] != '\0'; i++) {
        if (str[i] == '\n') {
            return true;
        }
    }
    return false;
}

int main(int argc, char** args)
{
    // Ignore pipe closure:
    signal(SIGPIPE, SIG_IGN);

    sleep_ms(10);
    // Are we creating?
    if (argc == 2 && strcmp(args[1], "init") == 0) {
        fprintf(stderr, "slow_cat(%d): Initializing semaphore.\n", getpid());
        initialize_sem();
        exit(EXIT_SUCCESS);
    }
    // Are we signalling?
    if (argc == 2 && strcmp(args[1], "signal") == 0) {
        fprintf(stderr, "slow_cat(%d): Signalling others to start.\n", getpid());
        signal_other_procs();
        exit(EXIT_SUCCESS);
    }
    // Are we waiting on a !
    if (argc == 2 && strcmp(args[1], "wait_on_!") == 0) {
        fprintf(stderr, "slow_cat(%d): Blocking script waiting for !\n", getpid());
        wait_till_done();
        fprintf(stderr, "slow_cat(%d):  --> done blocking script\n", getpid());
        exit(EXIT_SUCCESS);
    }

    // Help
    if (argc != 5) {
        fprintf(stderr, "usage: \n");
        fprintf(stderr, "   %s <file to send> <start delay ms> <burst size> <inter-burst ms>\n", args[0]);
        fprintf(stderr, "or\n");
        fprintf(stderr, "   %s init      = Reset the semaphore\n", args[0]);
        fprintf(stderr, "   %s signal    = Trigger all waiting processes\n", args[0]);
        fprintf(stderr, "   %s wait_on_! = Waits until one side sees '!\\n'\n", args[0]);
        exit(EXIT_FAILURE);
    }

    // Get the arguments
    char* file_name = args[1];
    int start_delay_ms = atoi(args[2]);
    int burst_size_lines = atoi(args[3]);
    int inter_line_delay_ms = atoi(args[4]);

    // open file
    FILE *file = fopen(file_name, "r");
    if (file == NULL) {
        fprintf(stderr, "slow_cat: Unable to open source file (%s)\n", file_name);
        return EXIT_FAILURE;
    }


    // Wait for sync
    fprintf(stderr, "slow_cat(%d): ... waiting to cat %s; %dms delay, %dms between...\n", 
        getpid(),
        file_name, start_delay_ms, inter_line_delay_ms);
    fflush(stderr);
    wait_till_signaled();

    // Start delay
    sleep_ms(start_delay_ms);
    fprintf(stderr, "slow_cat(%d): --> proceeding to cat\n", getpid());

    // Dump
    const int SIZE = 1024;
    char buff[SIZE];
    int burst_count = 0;

    while (fgets(buff, SIZE, file) != NULL) {
        // Delay a little before sending the "exit" string in case
        // the code under test responds to the !\n before finishing printing
        // the important end-row sentinal. 
        if (strcmp(buff, "!\n") == 0) {
            is_done = true;
            sleep_ms(start_delay_ms);
        }

        fputs(buff, stdout);
        // fprintf(stderr, "slow_cat(%d) out: %s\n", getpid(), buff);

        fflush(stdout);
        if (has_linefeed(buff)) {
            burst_count = (burst_count + 1) % burst_size_lines;
            if (burst_count == 0) {

                // static int num_reads = 0;
                // fprintf(stderr, "--> Pausing    #reads=%d\n", ++num_reads);

                sleep_ms(inter_line_delay_ms);
            }
        }

    }
    sleep_ms(inter_line_delay_ms);

    if (!is_done) {
        fprintf(stderr, "slow_cat(%d): Done; waiting for signal to exit\n", getpid());
        fflush(stderr);
        wait_till_done();
    } else {
        fprintf(stderr, "slow_cat(%d): Done; signalling other processes that printing done\n", getpid());
        fflush(stderr);
        signal_done();
    }
    return EXIT_SUCCESS;
}




/***********************************************************
 * Utility Functions
 ***********************************************************/
static void sleep_ms(int duration_ms)
{
	struct timespec req;
	req.tv_nsec = (duration_ms % 1000) * 1000 * 1000;
	req.tv_sec = (duration_ms / 1000);
	nanosleep(&req, NULL);
}
