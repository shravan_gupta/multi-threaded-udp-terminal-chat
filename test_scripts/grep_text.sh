#!/bin/bash

# Usage
if [ $# -ne 1 ] ; then
    echo "Search source code for needed and controlled calls."
    echo "Usage:"
    echo "    $0 student_s-talk_directory"
    echo "Example:"
    echo "    $0 ../as3/studentx"
    exit
fi
STDDIR=$1

STDFILES=$STDDIR/*.c

# add "-B3 -A3" option to display before/after
echo 
echo "**********************************"
echo "Searching for THREADS"
echo "**********************************"
echo "pthread_create"
grep -n 'pthread_create' $STDFILES

echo 
echo "**********************************"
echo "Searching for CONDITION VARIABLES"
echo "**********************************"
echo 'pthread_cond_t'
grep -n 'pthread_cond_t' $STDFILES
echo 'pthread_cond_wait'
grep -n 'pthread_cond_wait' $STDFILES
echo 'pthread_cond_signal'
grep -n 'pthread_cond_signal' $STDFILES

echo 
echo "**********************************"
echo "Searching for MUTEX LOCK / UNLOCK"
echo "**********************************"
echo "pthread_mutex_lock"
grep -n 'pthread_mutex_lock' $STDFILES
echo "pthread_mutex_unlock"
grep -n 'pthread_mutex_unlock' $STDFILES

echo 
echo "**********************************"
echo "Searching for return value checking"
echo "**********************************"
echo 'getaddrinfo'
grep -n 'getaddrinfo' $STDFILES
echo 'socket'
grep -n 'socket' $STDFILES
echo 'recvform'
grep -n 'recvfrom' $STDFILES
echo 'sendto'
grep -n 'sendto' $STDFILES

echo 
echo "**********************************"
echo "Searching for SLEEP"
echo "**********************************"
echo 'sleep'
grep -n -B4 -A4 'sleep' $STDFILES