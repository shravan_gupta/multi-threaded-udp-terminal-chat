#!/bin/bash
# Execute all the tests for the assignment

# Usage
if [ $# -ne 1 ] && [ $# -ne 2 ]; then
    echo "Usage:"
    echo "    $0 <student s-talk directory> [test # to run]"
    echo "Example:"
    echo "    $0 ../as3/studentx"
    echo "    $0 ../as3/studentx 1.2"
    exit
fi
STDDIR=$1
TEST_TO_RUN=$2

# Constants
BURST_LINES_SLOW=1
BURST_LINES_FAST=10
BURST_DELAY_ms=300
REPORT=outputs/report.txt

# Function for selecting which test to run
function ok_to_test()
{
    thisTestName=$1
    desiredTestPrefix=$2

    # Return true if no desired prefix given, or thisTestName starts with the desired prefix
    [ -z "$desiredTestPrefix" ] || [ $thisTestName == $desiredTestPrefix ]
}


# Clear
rm -rf outputs/
mkdir -p outputs/
mkdir -p zip_outputs/

# Search code
echo "Searching for required / restricted content:"
./grep_text.sh $STDDIR > outputs/grep_text_results.txt

# Summary Report
echo "Summary Report" > $REPORT
echo "   for dir '$STDDIR'" >> $REPORT
echo "" >> $REPORT

# 35 Code
#  5 Networking
# 20 Manual

# Basics
echo "" >> $REPORT
echo "[ 0] Basics (no deductions)" >> $REPORT
echo "---------------------------" >> $REPORT
ok_to_test "1.1" $TEST_TO_RUN  &&  ./execute.sh "1.1-HelloWorld"                    $STDDIR inputs/hw-1.txt inputs/blank.txt $BURST_LINES_SLOW $BURST_DELAY_ms "no"
ok_to_test "1.2" $TEST_TO_RUN  &&  ./execute.sh "1.2-HelloWorld-Exclamation"        $STDDIR inputs/hw-2.txt inputs/blank.txt $BURST_LINES_SLOW $BURST_DELAY_ms "no"
ok_to_test "1.3" $TEST_TO_RUN  &&  ./execute.sh "1.3-HelloWorld-ExclamationAtFront" $STDDIR inputs/hw-3.txt inputs/blank.txt $BURST_LINES_SLOW $BURST_DELAY_ms "no"
ok_to_test "1.4" $TEST_TO_RUN  &&  ./execute.sh "1.4-SlowRomeo"   $STDDIR inputs/romeo-28lines.txt   inputs/blank.txt $BURST_LINES_SLOW $BURST_DELAY_ms "no"

## Basic Files {fast}
echo "" >> $REPORT
echo "[15] Quickly " >> $REPORT
echo "-------------------" >> $REPORT
ok_to_test "2.1" $TEST_TO_RUN  &&  ./execute.sh "2.1-Romeo"   $STDDIR inputs/romeo-28lines.txt   inputs/blank.txt $BURST_LINES_FAST $BURST_DELAY_ms "no"
ok_to_test "2.2" $TEST_TO_RUN  &&  ./execute.sh "2.2-Hamlet"  $STDDIR inputs/hamlet-43lines.txt  inputs/blank.txt $BURST_LINES_FAST $BURST_DELAY_ms "no"
ok_to_test "2.3" $TEST_TO_RUN  &&  ./execute.sh "2.3-Laurier" $STDDIR inputs/laurier-75lines.txt inputs/blank.txt $BURST_LINES_FAST $BURST_DELAY_ms "no"

## Long Lines {fast}
echo "" >> $REPORT
echo "[ 5] Long Lines" >> $REPORT
echo "-----------------" >> $REPORT
ok_to_test "3.1" $TEST_TO_RUN  &&  ./execute.sh "3.1-Long-PnP"   $STDDIR inputs/pride-2000.txt inputs/blank.txt $BURST_LINES_FAST $BURST_DELAY_ms "no"
ok_to_test "3.2" $TEST_TO_RUN  &&  ./execute.sh "3.2-Long-Alice" $STDDIR inputs/alice-4000.txt inputs/blank.txt $BURST_LINES_FAST $BURST_DELAY_ms "no"

## Bidirectional {fast}
echo "" >> $REPORT
echo "[15] Bidirectional" >> $REPORT
echo "------------------" >> $REPORT
ok_to_test "4.1" $TEST_TO_RUN  &&  ./execute.sh "4.1-BiDir-Easy"   $STDDIR inputs/hamlet-43lines-noquit.txt  inputs/hamlet-43lines.txt  $BURST_LINES_FAST $BURST_DELAY_ms "no"
ok_to_test "4.2" $TEST_TO_RUN  &&  ./execute.sh "4.2-BiDir-Medium" $STDDIR inputs/laurier-75lines-noquit.txt inputs/laurier-75lines.txt $BURST_LINES_FAST $BURST_DELAY_ms "no"
ok_to_test "4.3" $TEST_TO_RUN  &&  ./execute.sh "4.3-BiDir-Hard"   $STDDIR inputs/sherlock-long-noquit.txt   inputs/sherlock-long.txt   $BURST_LINES_FAST $BURST_DELAY_ms "no"


##  Valgrind (of the 40 design/implementation marks)
echo "" >> $REPORT
echo "[ 5] Valgrind" >> $REPORT
echo "-----------------" >> $REPORT
ok_to_test "5.1" $TEST_TO_RUN  &&  ./execute.sh "5.1-Valgrind-UniDir" $STDDIR inputs/laurier-75lines.txt        inputs/blank.txt           $BURST_LINES_FAST $BURST_DELAY_ms "yes"
ok_to_test "5.2" $TEST_TO_RUN  &&  ./execute.sh "5.2-Valgrind-BiDir"  $STDDIR inputs/laurier-75lines-noquit.txt inputs/laurier-75lines.txt $BURST_LINES_FAST $BURST_DELAY_ms "yes"

echo "" >> $REPORT
echo "DONE" >> $REPORT

# Save files in ZIP
ZIP_NAME=Outputs-$(echo "$STDDIR" | tr /~[:space:] _).zip
echo "Zipping file $ZIP_NAME"
zip -r zip_outputs/$ZIP_NAME outputs/