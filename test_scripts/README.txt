Directions:

Marking should be done on SFU CSIL Linux machines

1. Run Manual Tests
    $ ./manual.sh rawA /path/to/student/folder x
    $ ./manual.sh rawB /path/to/student/folder x
    (the x must be there, but does not matter)
    (you then type on each terminal for input)

    Check for:
    1. Typing on one shows up on other, and vice-versa
       
       Ensure they don't prefix each received message with anything.
       (If they do, it will break the test suite; spend <5 min looking
       at code to remove their prefixing; otherwise leave student 
       feedback: "Received messages are prefixed with some text; please
       remove prefix and resubmit and email me")
    2. Typing "hi!\n" does not exit
    3. Typing "\n" works
    4. Typing "!\n" quits both sides

2. Run Network Tests
    From two computers on same network
    * Test with inaccessible socket (22)
    * Test with bad computer name ("no-such-computer")
    * Test with good connection, type on each end, '!\n' kills both

3. Run Test Suite
    (Takes about 2m to execute full suite)
    * Run the script
        $ ./as3_tests.sh /path/to/student/folder

    * If needed, run a specific test (such as 1.3)
        $ ./as3_tests.sh /path/to/student/folder 1.3

    * Look in the outputs/ folder for the results of all the test suite
        * outputs/ is deleted each time the script runs
        * Analyze the report.txt for tests which were not perfect 
          (look at .diff files for details)
        * Analyze the grep_text_results.txt for required / forbidden calls
    * It zips all the output into a file in the zip_outputs/ folder.
      When students have failures, consider attaching the ZIP file to CourSys

    * Marking: 
      -5 per failed tests; smaller deductions fine for smaller problems.
      Any time a student loses a mark, state why. Possibly: 
      "Failed test suite 1.2; see attached".
      (Consider making CourSys marking feedback buttons for this.)


    Troubleshooting Test Suite
    ----------------------------
    * Use test suite to run a specific test:
        $ $ ./as3_tests.sh /path/to/student/folder 1.3

    * Run each s-talk end of the communication without directing output to files (use 2 terminals)
      Uses slow-cat to send to each s-talk, much like test suite; must run A first.
        $ ./manual A /path/to/student/folder  inputs/hw-3.txt
        $ ./manual B /path/to/student/folder  inputs/blank.txt

    * Run fully manual input/output tests (from 2 terminals)
        $ ./manual rawA /path/to/student/folder x
        $ ./manual rawB /path/to/student/folder x
        (the x must be there, but does not matter)
        (you then type on each terminal for input)

    * If student's code fails to compile because missing instructorList.o, can copy the 
      provided one from online into their folder (no deduction).

    Test Suite Output Analysis
    - In the zip_outputs/ folder, one zip file is kept for each target folder (student)
    - In outputs/ look at the report.txt for a summary of any errors   
      Shows you if there was a difference between expected and actual output.
      Shows you if there were any interesting valgrind outputs
    - In outputs/ look at each of the .diff files to see details of errors.
    - Look at outputs/grep_text_results.txt for interesting highlights of the code
    - Look at real code too!

Description test suite input tool:
- slow_cat program feeds s-talk program input at a controlled rate.
    - uses a shared semaphore to control its start / end
    - call `slow_cat init` to initialize the sempaphores.
    - call `slow_cat ....` to prep sending data
    - call `slow_cat signal` to start all other slow_cat instances printing data
    - call `slow_cat wait_on_!` to wait until at least one slow_cat instance has encountered a "!\n"


