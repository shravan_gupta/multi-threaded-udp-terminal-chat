#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>


static bool line_is_match(char* line, char* check) 
{
    return 
        strncmp(line, check, strlen(check)) == 0
        && strlen(line) <= strlen(check) + 1;
}

int main(int argc, char** args)
{
    if (argc != 5) {
        fprintf(stderr, "usage: \n");
        fprintf(stderr, "   %s <source> <target> <start-line> <end-line>\n", args[0]);
        exit(EXIT_FAILURE);
    }

    // arguments
    char* source_file_name = args[1];
    char* target_file_name = args[2];
    char* start_line = args[3];
    char* end_line = args[4];

    // open files
    FILE *file = fopen(source_file_name, "r");
    if (file == NULL) {
        fprintf(stderr, "Unable to open source file (%s)\n", source_file_name);
        return EXIT_FAILURE;
    }
    FILE *target = fopen(target_file_name, "w");
    if (target == NULL) {
        fprintf(stderr, "Unable to open target file (%s)\n", target_file_name);
        return EXIT_FAILURE;
    }


    const int SIZE = 1024;
    char buff[SIZE];
    bool is_printing = false;
    while (fgets(buff, SIZE, file) != NULL) {
        if (!is_printing && line_is_match(buff, start_line)) {
            is_printing = true;
        }
        if (is_printing) {
            fputs(buff, target);
        }
        if (line_is_match(buff, end_line)) {
            break;
        }
    }
    fclose(file);
    fclose(target);
    return EXIT_SUCCESS;
}

