#!/bin/bash

# Usage
if [ $# -ne 7 ] ; then
    echo "Usage:"
    echo "    $0 results_dir std_dir source1 source2 burst_lines burst_delay use_valgrind"
    echo "Example:"
    echo "    $0 1-simple ../ src1.txt src2.txt 10 200 no"
    echo "    $0 8-valgrind-long ~/mark/studentx/ src1.txt src2.txt 10 200 yes"
    exit
fi

# Arguments
NAME=$1
STUDENT_DIR=$2
INPUT1=$3
INPUT2=$4
BURST_LINES=$5
BURST_DELAY=$6
USE_VALGRIND=$7

# Constants
PORT1=12345
PORT2=54321

OUTPUT1="outputs/tmp/output1.txt"
OUTPUT2="outputs/tmp/output2.txt"
REPORT="outputs/report.txt"
RESULTS1="outputs/tmp/results1.diff"
RESULTS2="outputs/tmp/results2.diff"
VALGRIND_OUT1="outputs/tmp/valgrind_out1.txt"
VALGRIND_OUT2="outputs/tmp/valgrind_out2.txt"
STALK_PATH=$STUDENT_DIR/s-talk

# Verify needed executables exist
verify_file_or_exit() {
    FILE=$1
    if [ ! -f "$FILE" ]; then
        echo "$FILE does not exist; please compile some code first?"
        exit
    fi
}
verify_file_or_exit ./slow_cat
verify_file_or_exit ./trim_file
verify_file_or_exit $STALK_PATH



# Cleanup from previous run
rm -fr outputs/tmp outputs/$NAME
mkdir -p outputs/tmp
killall slow_cat s-talk memcheck-amd64- 2> /dev/null


echo
echo "$NAME"
echo "----------------------------"

# Prep timing semaphores
./slow_cat init

# Begin running s-talk applications
if [ $USE_VALGRIND = 'yes' ]; then
    # under valgrind use longer startup delay because valgrind is slow to start
    echo "Valgrind running $INPUT1  <-->  $INPUT2..."
    ./slow_cat $INPUT1 2500 $BURST_LINES $BURST_DELAY | valgrind --leak-check=full --log-file=$VALGRIND_OUT1 $STALK_PATH $PORT1 localhost $PORT2 > $OUTPUT1 &
    ./slow_cat $INPUT2 2700 $BURST_LINES $BURST_DELAY | valgrind --leak-check=full --log-file=$VALGRIND_OUT2 $STALK_PATH $PORT2 localhost $PORT1 > $OUTPUT2 &
else
    echo "Running $INPUT1  <-->  $INPUT2..."
    ./slow_cat $INPUT1 200 $BURST_LINES $BURST_DELAY | $STALK_PATH $PORT1 localhost $PORT2 > $OUTPUT1 &
    ./slow_cat $INPUT2 400 $BURST_LINES $BURST_DELAY | $STALK_PATH $PORT2 localhost $PORT1 > $OUTPUT2 &
fi

# Wait for s-talk to start up
sleep 1
./slow_cat signal

# Wait until one slow_cat has sent the END message (sent !\n )
./slow_cat wait_on_!
sleep 1

# Strip off any extra startup/shutdown messages from s-talk
./trim_file $INPUT1 $INPUT1.trim +++++ -----
./trim_file $INPUT2 $INPUT2.trim +++++ -----
./trim_file $OUTPUT1 $OUTPUT1.trim +++++ -----
./trim_file $OUTPUT2 $OUTPUT2.trim +++++ -----

# Check if output matched expected input
# (Ignore all blanks)
diffOptions="--ignore-matching-lines=^\s*$ --ignore-blank-lines --ignore-all-space"
diff $diffOptions $INPUT1.trim $OUTPUT2.trim > $RESULTS1
diff $diffOptions $INPUT2.trim $OUTPUT1.trim > $RESULTS2

# Report
echo "$NAME" >> $REPORT
if [ ! -f $RESULTS1 ]; then
    echo "    !!! FAILED: Results file not found: $RESULTS1" >> $REPORT
fi
if [ ! -f $RESULTS2 ]; then
    echo "    !!! FAILED: Results file not found: $RESULTS2" >> $REPORT
fi
if [ -s $RESULTS1 ]; then
    wc -l $RESULTS1 | awk '{print  "   >>> FAILED <<<  ", $1, "lines in diff:", $RESULTS1 }' >> $REPORT
    head -n 5 $RESULTS1 | sed 's/^/      /' >> $REPORT
fi
if [ -s $RESULTS2 ]; then
    wc -l $RESULTS2 | awk '{print  "   >>> FAILED <<<  ", $1, "lines in diff:", $RESULTS2 }' >> $REPORT
    head -n 5 $RESULTS2 | sed 's/^/      /' >> $REPORT
fi

# Report - Valgrind
if [ $USE_VALGRIND = 'yes' ]; then
    grep -Hn "definitely lost" $VALGRIND_OUT1 $VALGRIND_OUT2 >> $REPORT
    grep -Hn "Invalid write of size" $VALGRIND_OUT1 $VALGRIND_OUT2 >> $REPORT
    grep -Hn "Invalid read of size" $VALGRIND_OUT1 $VALGRIND_OUT2 >> $REPORT
    grep -Hn "still reachable" $VALGRIND_OUT1 $VALGRIND_OUT2 >> $REPORT
fi

# Save results
mv outputs/tmp outputs/$NAME

# Extra delay at end for slow to shut-down programs
sleep 2
