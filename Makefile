
CFLAGS = -Werror -Wall -g -std=c11 -D _POSIX_C_SOURCE=200809L -pthread

build: list.o s-talk.o 
	gcc $(CFLAGS) s-talk.o list.o -o s-talk

list.o: list.c list.h
	gcc $(CFLAGS) -c list.c
	
s-talk.o: s-talk.c s-talk.h
	gcc $(CFLAGS) -c s-talk.c
	
valgrind: build
	valgrind --leak-check=full --show-leak-kinds=all --track-origins=yes ./s-talk 2001 localhost 3001
	
clean:
	rm s-talk *o