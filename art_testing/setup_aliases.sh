#!/bin/bash
# LOAD THIS BY:

# path to this folder of scripts
my_dir=~/a3_lnk

echo "You must load this file via source (if you have not done so already):"
echo "    source $0"
echo "Test script folder: $my_dir"

alias sextract="unzip -j *.zip"                         # Extract without folders
alias scode="$my_dir/grep_text.sh . | leafpad"          # View (in leafpad) ouput of grepping interesting stuff in code
alias sman="$my_dir/manual.sh . no_valgrind"            # Run without valgrind
alias snetwork="$my_dir/network.sh ."                   # Check networking 
alias sart="$my_dir/manual.sh ."                        # Run with valgrind
alias sgrind="$my_dir/grep_valgrind_output.sh ."        # Check valgrind output (cat the file if it's unclear)
