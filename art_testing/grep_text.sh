#!/bin/bash

# Usage
if [ $# -ne 1 ] ; then
    echo "Search source code for needed and controlled calls."
    echo "Usage:"
    echo "    $0 student_s-talk_directory"
    echo "Example:"
    echo "    $0 ../as3/studentx"
    exit
fi
STDDIR=$1

cd $STDDIR

# add "-B3 -A3" option to display before/after
echo 
echo "**********************************"
echo "Searching for THREADS"
echo "**********************************"
echo "pthread_create"
grep -n -r --include \*.c 'pthread_create'

echo 
echo "**********************************"
echo "Searching for List"
echo "**********************************"
echo "List_create"
grep -n -r --include \*.c --exclude list.c 'List_create'
echo "List_{add, prepend, insert, append}"
grep -n -r --include \*.c --exclude list.c 'List_add'
grep -n -r --include \*.c --exclude list.c 'List_prepend'
grep -n -r --include \*.c --exclude list.c 'List_insert'
grep -n -r --include \*.c --exclude list.c 'List_append'



echo 
echo "**********************************"
echo "Searching for CONDITION VARIABLES"
echo "**********************************"
echo 'pthread_cond_t'
grep -n -r --include \*.c 'pthread_cond_t'
echo 'pthread_cond_wait'
grep -n -r --include \*.c 'pthread_cond_wait'
echo 'pthread_cond_signal'
grep -n -r --include \*.c 'pthread_cond_signal'

echo 
echo "**********************************"
echo "Searching for MUTEX LOCK / UNLOCK"
echo "**********************************"
echo "pthread_mutex_lock"
grep -n -r --include \*.c 'pthread_mutex_lock'
echo "pthread_mutex_unlock"
grep -n -r --include \*.c 'pthread_mutex_unlock'

echo 
echo "**********************************"
echo "Searching for return value checking"
echo "**********************************"
echo 'getaddrinfo'
grep -n -r --include \*.c 'getaddrinfo'
echo 'socket'
grep -n -r --include \*.c 'socket'
echo 'recvform'
grep -n -r --include \*.c 'recvfrom'
echo 'sendto'
grep -n -r --include \*.c 'sendto'

echo 
echo "**********************************"
echo "Searching for SLEEP"
echo "**********************************"
grep -n -r --include \*.c -B4 -A4 'sleep'