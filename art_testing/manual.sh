#!/bin/bash
# Launch two s-talk applications.

if [ $# -lt 1 ] ; then
	echo "Open two s-talk applications (one in a new window)"
	echo
    echo "Usage with Valgrind (output written to output_val#.txt):"
    echo "    $0  <path to student dir> "
    echo "Usage without Valgrind"
    echo "    $0  <path to student dir> no_valgrind"
    echo "Examples:"
    echo "    $0 ../as3/studentx        "
    echo "    $0 ../as3/studentx no_valgrind"
    echo
    exit
fi

# Arguments:
stalk_path=$1
skip_valgrind=$2	# Any value here is fine to trigger no valgind


# Function to wait until port is free.
wait_on_port() {
	port=$1

	while :
	do
	    output=$(lsof -t -i:$port)
	    if [ -z "$output" ]
	    then
		return 1
	    fi
	    echo "wait_on_port.sh: Port $port in use for PID $output"
	    sleep 1
	done
}


# Cleanup any previous instances
killall -9 slow_cat s-talk memcheck-amd64- 2> /dev/null
wait_on_port 12345
wait_on_port 54321

# Who are we calling ourselves?
localhost="localhost"
# localhost="Ubuntu20"
# localhost="127.0.0.1"

if [ -z "$skip_valgrind" ] 
then
	echo "Manual test -- Spawning two s-talk with Valgrind"
	VALGRIND_OUT1=output_val1.txt
	VALGRIND_OUT2=output_val2.txt

	# Start both ends
	xterm -hold -geometry 250x45+1000+0 -e "valgrind --leak-check=full --show-leak-kinds=all --log-file=$VALGRIND_OUT1 $stalk_path/s-talk 54321 $localhost 12345; echo '--DONE--'" &
	valgrind --leak-check=full --show-leak-kinds=all --log-file=$VALGRIND_OUT2 $stalk_path/s-talk 12345 $localhost 54321
else
	# Start both ends no valgrind
	echo "Manual test -- Spawning two s-talk   ** NO VALGRIND **"
	xterm -hold -geometry 250x45+1000+0 -e "$stalk_path/s-talk 54321 $localhost 12345; echo '--DONE--'" &
	$stalk_path/s-talk 12345 $localhost 54321
fi
echo "--DONE--"

