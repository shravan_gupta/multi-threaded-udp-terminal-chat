/*
s-talk.c

Author: Shravan Gupta
Date: 26-6-20
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>			// for memset()
#include <unistd.h>			// for close()

#include <sys/types.h>      // for getaddrinfo
#include <sys/socket.h>     // for getaddrinfo
#include <netdb.h>
#include <pthread.h>

#include "s-talk.h"
#include "list.h"

#define MSG_MAX_LEN 1024

static int socketDescriptor;
static struct addrinfo* result;

static List* transmitList;
static List* receiveList;

static int transmitDone = 1;
static int receiveDone = 1;

static pthread_t thread[4];
static pthread_mutex_t transmitMutex = PTHREAD_MUTEX_INITIALIZER;    
static pthread_mutex_t receiveMutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t transmitListFullCond = PTHREAD_COND_INITIALIZER;
static pthread_cond_t transmitListEmptyCond = PTHREAD_COND_INITIALIZER;
static pthread_cond_t receiveLisFullCond = PTHREAD_COND_INITIALIZER;
static pthread_cond_t receiveListEmptyCond = PTHREAD_COND_INITIALIZER;


// Setup local UDP socket and bind it to specified port
void setupLocalUDP(int  myPort) {

    // Address
	struct sockaddr_in sin;
	memset(&sin, 0, sizeof(sin));
	sin.sin_family = AF_INET;                   // Connection may be from network
	sin.sin_addr.s_addr = htonl(INADDR_ANY);    // Host to Network long
	sin.sin_port = htons(myPort);               // Host to Network short
	
	// Create the socket for UDP 
    socketDescriptor  = socket(PF_INET, SOCK_DGRAM, 0);;
  	if(socketDescriptor == -1){
          perror("Error creating a socket");
          exit(1);
      }

	// Bind the socket to the port (PORT) that we specify
	if(bind (socketDescriptor, (struct sockaddr*) &sin, sizeof(sin)) == -1){
        perror("Error binding socket");
        close(socketDescriptor);
        exit(1);
    }
}

// Setup remote UPD socket with specified port
void setupRemoteUDP(char* remoteName, char* remotePort) {

    // Address
	struct addrinfo hints;
	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_DGRAM;

    int remoteInfo = getaddrinfo(remoteName, remotePort, &hints, &result); 
    if(remoteInfo != 0){
        close(socketDescriptor);
        fprintf(stderr, "Error getting remote info: %s\n", gai_strerror(remoteInfo));
        exit(1);
    }
}

void freeItem(void* item) {
  if (item != NULL) {
    free(item);
  }
}

// Thread 1 reads the input from the keyboard
void* readFromKeyboardThread(void* unused) {

    // Allocate MSG_MAX_LEN + 1 as fgets adds '\0' automatically at the end
    char messageRx[MSG_MAX_LEN + 1];
    while(1) {
		
        if(List_count(transmitList) == LIST_MAX_NUM_NODES) {
            perror("Error, transmit list is full...");
            exit(0);
        }

        // Wait till transmitThread is done
        pthread_mutex_lock(&transmitMutex); 
        while(transmitDone == 0) {
            pthread_cond_wait(&transmitListEmptyCond, &transmitMutex);
        }
        pthread_mutex_unlock(&transmitMutex);

        // Clear the buffer and read data from the keyboard
        memset(messageRx, '\0', MSG_MAX_LEN);
        if(fgets(messageRx, MSG_MAX_LEN, stdin) == NULL) {
            perror("Error receiving message from local process");
            exit(1);
        }
        // Prepend the messge to the list and signal blocked processes to resume
        else {
            pthread_mutex_lock(&transmitMutex);
            List_prepend(transmitList, messageRx);
            pthread_cond_signal(&transmitListFullCond);
            transmitDone = 0;
            pthread_mutex_unlock(&transmitMutex);
            
            printf("Local User: %s", messageRx);

            // Terminate the app if '!' is entered
            if (messageRx[0] == '!' && messageRx[1] == '\n') {
                printf("\nSession terminated by local user...\nThank you for using s-talk app!\n");
                pthread_cancel(thread[1]);
                pthread_cancel(thread[2]);
                break;
            }
        }
    }
    pthread_exit(NULL);
}

// Thread 2 receives message from the remote process
void* receiveThread(void* unused) {

    char messageRx[MSG_MAX_LEN];
    while(1) {
		
        if(List_count(receiveList) == LIST_MAX_NUM_NODES) {
            perror("Error, receive list is full...");
            exit(0);
        }
		
        // Address
   		struct sockaddr_in sinRemote;
		unsigned int sin_len = sizeof(sinRemote);

        // Wait till writeToScreenThread is done
        pthread_mutex_lock(&receiveMutex);
        while(receiveDone == 0) {
            pthread_cond_wait(&receiveListEmptyCond, &receiveMutex);
        }
        pthread_mutex_unlock(&receiveMutex);

        // Receive the data
        int numBytes = recvfrom(socketDescriptor, messageRx, MSG_MAX_LEN, 0, (struct sockaddr *) &sinRemote, &sin_len);
        
        // Make it null terminated (so string functions work)
		int terminateIdx = (numBytes < MSG_MAX_LEN) ? numBytes : MSG_MAX_LEN - 1;
		messageRx[terminateIdx] = 0;
        
        if(numBytes == -1) {
            perror("Error receiving message from remote process");
            exit(1);
        }
        // Prepend the messge to the list and signal blocked processes to resume
        else{
            pthread_mutex_lock(&receiveMutex);
            List_prepend(receiveList, messageRx);
            pthread_cond_signal(&receiveLisFullCond);
            receiveDone = 0;
            pthread_mutex_unlock(&receiveMutex);

            // Terminate the app if '!' is entered
            if (messageRx[0] == '!' && messageRx[1] == '\n') {
                printf("\nSession terminated by remote user...\nThank you for using s-talk app!\n");
                pthread_cancel(thread[0]);
                pthread_cancel(thread[3]);
                break;
            }
        }
    }
    pthread_exit(NULL);
}

// Thread 3 prints the message to the screen
void* writeToScreenThread(void* unused) {
    while(1) {
        
        // Wait till receiveThread is done and receives input
        pthread_mutex_lock(&receiveMutex);
        while(List_count(receiveList) == 0) {
            pthread_cond_wait(&receiveLisFullCond, &receiveMutex);
        }

        // Trim the messge from the list and signal blocked processes to resume
        if(List_count(receiveList) > 0) {
            char* messageTx = List_trim(receiveList);
            pthread_cond_signal(&receiveListEmptyCond);
            receiveDone = 1;
            pthread_mutex_unlock(&receiveMutex);
            
            // Terminate the app if '!' is entered
            if(messageTx[0] == '!' && messageTx[1] == '\n' ) {
                break;
            }
            //printf("Remote User: ");
            fputs(messageTx, stdout);
        }
    }
    pthread_exit(NULL);
}

// Thread 4 transmits the message to remote process
void* transmitThread(void* unused) {
    while(1) {

        // Wait while transmit list received input
        pthread_mutex_lock(&transmitMutex);
        while(List_count(transmitList) == 0) {
            pthread_cond_wait(&transmitListFullCond, &transmitMutex);
        }

        // Trim the messge from the list and signal blocked processes to resume
        if(List_count(transmitList) > 0) {
            char* messageTx  = List_trim(transmitList);

            int numBytes = sendto(socketDescriptor, messageTx, strlen(messageTx), 0, result->ai_addr, result->ai_addrlen);
            if(numBytes == -1){
                perror("Error sending message to remote process");
                exit(1);
            }

            pthread_cond_signal(&transmitListEmptyCond);
            transmitDone = 1;
            pthread_mutex_unlock(&transmitMutex); 
            
            // Terminate the app if '!' is entered
            if(messageTx[0] == '!' && messageTx[1] == '\n') {
                break;
            }
        }
    }
    pthread_exit(NULL);
}

int main(int argc, char* argv[])
{
    if(argc != 4) {
        fprintf(stderr, "Usage: s-talk [my port number] [remote machine name] [remote port number] \n");
        exit(1);
    }

    // argv[1]: myPort; argv[2]: remoteName; argv[3]: remotePort
    int myPort = atoi(argv[1]);
    char* remoteName = argv[2];
    char* remotePort = argv[3];
    
    // Local Socket Setup
    printf("Setting up Local Socket, listening at: %d\n", myPort);
    setupLocalUDP(myPort);
    printf("Success setting up Local Socket!\n");

    // Remote Socket Setup
    printf("Setting up remote Socket, connecting to: %s\n", remoteName);
    setupRemoteUDP(remoteName, remotePort);  
    printf("Success setting up remote Socket!\n");

    printf("Initializing...\n");
    printf("Please type something to start and enter '!' to exit...\n");

    // Initialize lists
    transmitList = List_create();
    receiveList = List_create();

    // Initialize threads
    pthread_create(&thread[0], NULL, readFromKeyboardThread, NULL);
    pthread_create(&thread[1], NULL, receiveThread, NULL);
    pthread_create(&thread[2], NULL, writeToScreenThread, NULL);
    pthread_create(&thread[3], NULL, transmitThread, NULL);

    // Wait for other threads to finish then cleanup
    for (int i = 0; i < 4; i ++) {
        pthread_join(thread[i], NULL);
    }

    // Cleanup
    pthread_mutex_destroy(&transmitMutex);
    pthread_mutex_destroy(&receiveMutex);
    pthread_cond_destroy(&transmitListFullCond);
    pthread_cond_destroy(&transmitListEmptyCond);
    pthread_cond_destroy(&receiveLisFullCond);
    pthread_cond_destroy(&receiveListEmptyCond);
    freeaddrinfo(result);
    close(socketDescriptor);
    List_free(transmitList, freeItem);
    List_free(receiveList, freeItem);
    return 0;
}
