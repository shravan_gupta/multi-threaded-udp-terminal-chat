/*
s-talk.h

Author: Shravan Gupta
Date: 26-6-20
*/

#ifndef _STALK_H_
#define _STALK_H_

// Setup local UDP socket and bind it to specified port
void setupLocalUDP(int myPort); 

// Setup remote UPD socket with specified port
void setupRemoteUDP(char* remoteName, char* remotePort);

// Thread 1 awaits input from the keyboard
void* readFromKeyboardThread(void* unused);

// Thread 2 awaits a UPD datagram
void* receiveThread(void* unused);

// Thread 3 prints the characters to the screen
void* writeToScreenThread(void* unused);

// Thread 4 transmits the data to remote process
void* transmitThread(void* unused);

#endif
