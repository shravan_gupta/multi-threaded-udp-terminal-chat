# Marking Process

For the s-talk application, here is the simplified marking process. It does not capture
s-talk's output or do any comparisons, nor does it do anything clever about sending
input to s-talk. It's call the "art" tests because it pastes ASCII art into s-talk.

Let /path/to/tests/ be where these tests are located.


## Process

- Enter student's folder check for LATE.txt; apply 5%/day late; 2h grace.
- Extract archive; build.
- Check code for required / forbidden things:
	$ /path/to/tests/grep_text.sh .

- Basic test without valgrind:
	$ /path/to/tests/manual.sh . no_valgrind
	
	Enter text into s-talk:
		hello world
		
		testing
		!at start
		at end!
		!
		
- Test networking:
	$ /path/to/tests/network.sh .
	
	Should display message about bad port and exit
	Should display message about bad host and exit
	(OK if message is not great)
	-2 if program still runs
	-3 if it segfaults

	Should then spawn two connected s-talk applications using the hostname.
	Type in something "hello world" and prove it appears. Type "!\n" to exit.
		
- Ascii Art test
	$ /path/to/tests/manual.sh .
	Copy paste each ASCII art file's content.
	(tip: open art in gedit, select text to copy, middle-mouse click into s-talk to paste)

	Try pasting numerous times quite quickly
	
	Apply deduction for any corruption in the image. Extra blank lines and prefixes OK.

	If s-talk is too slow under valgrind, apply a deduction and retry test
	without valgrind.

- Check valgrind outputs from the valgrind tests:
	$ cat output_val1.txt
	$ cat output_val2.txt

	Deductions:	
	(Applied at most once per student, even though there are two valgrind files to process)
	-0 No deduction for memory still accessible from the pthread_cancel_init function.
	-5 Other memory still accessible
	-5 Memory leak
	-3 Invalid memory accesses (read/write/uninitialized memory use)



## Shortcuts (alias)

- Edit the setup_aliases.sh file to change the "my_dir" to be the path  
  of the test scripts

- In your terminal, activate the aliases:
	$ source setup_aliases.sh

- See setup_aliases.sh for description of commands. 
  Once in the student's folder, you can test with these commands:
	$ sextract
	$ make
	$ scode
	$ sman
	$ snetwork
	$ sart
	$ sgrind