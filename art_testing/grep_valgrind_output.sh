#!/bin/bash
# Process the two valgrind log files into something brief to give you a hint
# if you need to look at the full logs or can ignore it.
stalk_path=$1

grep -HnE 'definitely lost| write of size|Invalid read of size|still reachable|pthread_cancel_init|getaddrinfo|pthread_create' $stalk_path/output_val*.txt
