#!/bin/bash
# Simple networking checks

if [ $# -lt 1 ] ; then
    echo "Usage:"
    echo "    $0  <path to student dir> "
    echo
    echo "    $0 ../as3/studentx "
    echo
    exit
fi


stalk_path=$1

# Cleanup
killall -9 slow_cat s-talk memcheck-amd64- 2> /dev/null

# Check bad port # (protected) behaviour:
echo "Deduct 2 marks if does not print some error message and exit"
echo "Deduct 3 marks if segfaults."
echo
echo "--> Running on protected port: 22 (local)"
$stalk_path/s-talk 22 localhost 412

# Check bad host behaviour
echo
echo
echo
echo "--> Running on bad host: nobody-by-that-name"
$stalk_path/s-talk 12345 nobody-by-that-name 54321

# Check working networking based on the local computer name.
echo
echo
echo
host=$(hostname)
echo "--> Running on $host"
xterm -geometry 250x45+1000+0 -e "$stalk_path/s-talk 54321 $host 12345" &
$stalk_path/s-talk 12345 $host 54321

